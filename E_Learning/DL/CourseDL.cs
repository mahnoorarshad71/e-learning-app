﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using E_learning.BL;
using System.IO;
namespace E_learning.DL
{
    internal class CourseDL
    {
        public static List<Course> course = new List<Course>();
        public static string path = "C:\\Users\\M\\source\\repos\\E_learning\\courses.txt";
        static void Sortcourse(List<Course> course)
        {
            course = course.OrderByDescending(o => o.price).ToList();
        }
        public static Course getCourse(string CourseID)
        {
            foreach(Course course in course)
            {
                if(CourseID==course.ID)
                {
                    return course;
                }
            }
            return null;
        }
        public void updateCourse(int field)
        {
            if(field == 0)
            {
            }
            else if(field == 1)
            {

            }
            else if (field == 2)
            {

            }
            else if (field == 3)
            {

            }
            else if(field==4)
            {

            }
        }
        public static void storeCourse()
        {

            StreamWriter courses = new StreamWriter(path, true);
            foreach (Course i in course)
            {

                courses.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7}", i.Name,i.ID, i.Description, i.sLimit, i.Snum, i.price, i.T_status,i.totalattendence);
            }
            courses.Flush();
            courses.Close();
        }
        public static void loadcourse()
        {
            StreamReader courses = new StreamReader(path);
            string line;
            string[] parts = new string[8];
            string Name, ID, Description;
            int sLimit;
            float price;
            while ((line= courses.ReadLine()) != null) 
            {
                parts = line.Split(',');
                Name = parts[0];
                ID = parts[1];
                Description = parts[2];
                sLimit = int.Parse(parts[3]);
                price = float.Parse(parts[5]);
                Course c = new Course(Name, ID, Description, price, sLimit);
                c.Snum = int.Parse(parts[4]);
                
                c.T_status = int.Parse(parts[6]);
                c.totalattendence = float.Parse(parts[7]);
                course.Add(c);
                
            }
            

                courses.Close();
        }
        public void updateCourseName(Course course, string name)
        {
            course.Name = name;
        }
        public void updateCourseDescription(Course course, string description)
        {
        course.Description = description;
        }
        public void updateCoursePrice(Course course, float price)
        {
            course.price = price;
        }
        public void updateCourseDescription(Course course, int sLimit)
        {
        course.sLimit = sLimit;
        }

        
        
    }
}
