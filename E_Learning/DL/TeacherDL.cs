﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using E_learning.BL;
using System.IO;
namespace E_learning.DL
{
    internal class TeacherDL
    {
        public static List<Teacher> teacher = new List<Teacher>();
        public static string path = "C:\\Users\\M\\source\\repos\\E_learning\\teachers.txt";
        public static  Teacher getTeacher(string teacherID)
        {
            foreach(Teacher teacher in teacher)
            {
                if(teacher.ID == teacherID)
                {
                    return teacher;
                }
            }
            return null;
        }
        static void addgrades(List<Student> student,Teacher t)
        {
            foreach(Student s in student)
            {
                foreach (Course c in s.registeredCourses)
                {
                    if (t.teachingCourse == c)
                    {
                        s.Grade = Console.ReadLine();
                    }
                }
            }
            

        }
         
       public static Teacher validteacher(MUser m)
        {
            foreach(Teacher k in teacher)
            {

                if (m.name ==k.name &&m.password == k.password)
                {
                    if (k.jobStatus)
                    {
                        return k;
                    }

                }

            }
            return null;

        }
        public static void storeteacher()
        {
            try
            {
                StreamWriter teachers = new StreamWriter(path, true);
                foreach (Teacher j in teacher)
                {
                    teachers.WriteLine("{0},{1},{2},{3},{4},{5},{6},[{7}", j.name, j.Email, j.password, j.Qualification, j.Phone, j.jobStatus, j.teachingCourse.ID, j.Salary);
                }
                teachers.Flush();
                teachers.Close();
            }
            catch (Exception ex)
            { Console.WriteLine(ex.Message); }
        }
        public static void addTeacherIntoList(Teacher t)
        {
            teacher.Add(t);
        }
        public static void loadTeacher()
        {
            string name,password,email,qualification="";
            ulong phone;
            bool jobStatus;
            Course c = null;
            string[] parts = new string[7];
            string line;
            StreamReader teachers = new StreamReader(path);
            while ((line =  teachers.ReadLine() ) != null)
            {
               
                parts = line.Split(',');
                name = parts[0];
                email = parts[1];
                password =parts[2];
                qualification = parts[3];
                phone = ulong.Parse(parts[4]);
                jobStatus = bool.Parse(parts[5]);
                if (jobStatus)
                {
                    c = CourseDL.getCourse(parts[6]);
                }
                Teacher t = new Teacher(name,password);
                if(c!=null)
                {
                    c.teacher = t;
                }
                t.teachingCourse = c;
                t.Email= email;
                t.Qualification = qualification;
                t.Phone = phone;
                t.jobStatus = jobStatus;
                t.Salary=float.Parse(parts[7]);
                teacher.Add(t);
             
            }
        }
        public static void getpayment(float salary)
        {
            if (salary != 0)
            {
                Console.WriteLine("Your salary is deposited.");
                Console.Write("Amount  :  ");
                Console.WriteLine(salary);
            }
            else
            {
                Console.WriteLine("Your salary has not been deposited by admin");
            }

        }
        public static void addattendence(int cnum, List<Course> course, List<Student> student, ref float totalattendence)
        {
            char attendence;
            for (int i = 0; i < course[cnum].Snum; i++)
            {
                Console.Write("Enter attendence of student (P for present or A for absent) ");
                Console.WriteLine(i + 1);
                Console.WriteLine(": ");
              //  Console.WriteLine(student[cnum].Name);
                attendence = char.Parse(Console.ReadLine());

                if (attendence == 'p' || attendence == 'P')
                {
                    for (int j = 0; j < student.Count; j++)
                    {
                      //  if (student[j].cNum == cnum)
                        {
                            student[j].attendence++;
                        }
                    }
                }

            }
            totalattendence = totalattendence + 1;

        }
    }
}
