﻿namespace E_Learning
{
    partial class viewCourse
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.coursesGV = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.coursesGV)).BeginInit();
            this.SuspendLayout();
            // 
            // coursesGV
            // 
            this.coursesGV.AllowUserToAddRows = false;
            this.coursesGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.coursesGV.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.coursesGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.coursesGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.coursesGV.Dock = System.Windows.Forms.DockStyle.Right;
            this.coursesGV.Location = new System.Drawing.Point(0, 0);
            this.coursesGV.Margin = new System.Windows.Forms.Padding(2);
            this.coursesGV.Name = "coursesGV";
            this.coursesGV.RowHeadersWidth = 62;
            this.coursesGV.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.coursesGV.RowTemplate.Height = 28;
            this.coursesGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.coursesGV.ShowEditingIcon = false;
            this.coursesGV.Size = new System.Drawing.Size(629, 479);
            this.coursesGV.TabIndex = 2;
            this.coursesGV.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.usersGV_CellContentClick_1);
            // 
            // viewCourse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.coursesGV);
            this.Name = "viewCourse";
            this.Size = new System.Drawing.Size(629, 479);
            ((System.ComponentModel.ISupportInitialize)(this.coursesGV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.DataGridView coursesGV;
    }
}
