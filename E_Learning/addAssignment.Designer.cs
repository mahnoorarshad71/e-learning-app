﻿namespace E_Learning
{
    partial class addAssignment
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnaddCourse = new System.Windows.Forms.Button();
            this.txtname = new System.Windows.Forms.TextBox();
            this.txtduedate = new System.Windows.Forms.TextBox();
            this.txtTMarks = new System.Windows.Forms.TextBox();
            this.txtdescription = new System.Windows.Forms.TextBox();
            this.lblseats = new System.Windows.Forms.Label();
            this.lblduedate = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.lblname = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnaddCourse
            // 
            this.btnaddCourse.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddCourse.Location = new System.Drawing.Point(463, 421);
            this.btnaddCourse.Name = "btnaddCourse";
            this.btnaddCourse.Size = new System.Drawing.Size(118, 37);
            this.btnaddCourse.TabIndex = 53;
            this.btnaddCourse.Text = "Add";
            this.btnaddCourse.UseVisualStyleBackColor = true;
            this.btnaddCourse.Click += new System.EventHandler(this.btnaddCourse_Click);
            // 
            // txtname
            // 
            this.txtname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.Location = new System.Drawing.Point(52, 49);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(248, 30);
            this.txtname.TabIndex = 52;
            // 
            // txtduedate
            // 
            this.txtduedate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtduedate.Location = new System.Drawing.Point(52, 194);
            this.txtduedate.Name = "txtduedate";
            this.txtduedate.Size = new System.Drawing.Size(248, 30);
            this.txtduedate.TabIndex = 51;
            // 
            // txtTMarks
            // 
            this.txtTMarks.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTMarks.Location = new System.Drawing.Point(52, 267);
            this.txtTMarks.Name = "txtTMarks";
            this.txtTMarks.Size = new System.Drawing.Size(248, 30);
            this.txtTMarks.TabIndex = 50;
            // 
            // txtdescription
            // 
            this.txtdescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdescription.Location = new System.Drawing.Point(52, 121);
            this.txtdescription.Name = "txtdescription";
            this.txtdescription.Size = new System.Drawing.Size(248, 30);
            this.txtdescription.TabIndex = 48;
            // 
            // lblseats
            // 
            this.lblseats.AutoSize = true;
            this.lblseats.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblseats.Location = new System.Drawing.Point(47, 239);
            this.lblseats.Name = "lblseats";
            this.lblseats.Size = new System.Drawing.Size(166, 25);
            this.lblseats.TabIndex = 47;
            this.lblseats.Text = "Enter Total Marks";
            // 
            // lblduedate
            // 
            this.lblduedate.AutoSize = true;
            this.lblduedate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblduedate.Location = new System.Drawing.Point(47, 166);
            this.lblduedate.Name = "lblduedate";
            this.lblduedate.Size = new System.Drawing.Size(145, 25);
            this.lblduedate.TabIndex = 45;
            this.lblduedate.Text = "Enter Due Date";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescription.Location = new System.Drawing.Point(47, 93);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(160, 25);
            this.lblDescription.TabIndex = 44;
            this.lblDescription.Text = "Enter Description";
            // 
            // lblname
            // 
            this.lblname.AutoSize = true;
            this.lblname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblname.Location = new System.Drawing.Point(47, 21);
            this.lblname.Name = "lblname";
            this.lblname.Size = new System.Drawing.Size(115, 25);
            this.lblname.TabIndex = 43;
            this.lblname.Text = "Enter Name";
            // 
            // addAssignment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnaddCourse);
            this.Controls.Add(this.txtname);
            this.Controls.Add(this.txtduedate);
            this.Controls.Add(this.txtTMarks);
            this.Controls.Add(this.txtdescription);
            this.Controls.Add(this.lblseats);
            this.Controls.Add(this.lblduedate);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblname);
            this.Name = "addAssignment";
            this.Size = new System.Drawing.Size(629, 479);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnaddCourse;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.TextBox txtduedate;
        private System.Windows.Forms.TextBox txtTMarks;
        private System.Windows.Forms.TextBox txtdescription;
        private System.Windows.Forms.Label lblseats;
        private System.Windows.Forms.Label lblduedate;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label lblname;
    }
}
