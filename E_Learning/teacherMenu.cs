﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using E_learning.BL;
using E_learning.DL;
namespace E_Learning
{
    public partial class teacherMenu : Form
    {
        Teacher t;
        home homeui;
        addAttendence attendence;
        addinfo info;
        salaryview salary;
        addAssignment assignment;
        addClass Class;
        public teacherMenu(Teacher t)
        {
            info = new addinfo(t);
            attendence = new addAttendence(t);
            this.t = t;
            InitializeComponent();
            homeui = new home();
            salary = new salaryview(t);
            assignment = new addAssignment(t);
            Class = new addClass(t);
        }

        private void teacherMenu_Load(object sender, EventArgs e)
        {
            List<Student> students = StudentDL.getStudentsofCourse(t.teachingCourse);
            attendence.studentsattendenceGV.DataSource = StudentDL.students;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!panel2.Controls.Contains(homeui))
            {
                panel2.Controls.Add(homeui);
                homeui.Dock = DockStyle.Fill;
                homeui.BringToFront();
            }
            else
            {
                homeui.BringToFront();
            }

        }
        
        private void button8_Click(object sender, EventArgs e)
        {
            if (!panel2.Controls.Contains(assignment))
            {
                panel2.Controls.Add(assignment);
                assignment.Dock = DockStyle.Fill;
                assignment.BringToFront();
            }
            else
            {
                assignment.BringToFront();
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (!panel2.Controls.Contains(attendence))
            {
                panel2.Controls.Add(attendence);
                attendence.Dock = DockStyle.Fill;
                attendence.BringToFront();
            }
            else
            {
                attendence.BringToFront();
            }
        }

        private void btnaddinfo_Click(object sender, EventArgs e)
        {
            if (!panel2.Controls.Contains(info))
            {
                panel2.Controls.Add(info);
                info.Dock = DockStyle.Fill;
                info.BringToFront();
            }
            else
            {
                info.BringToFront();
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            //acherDL.storeteacher();
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!panel2.Controls.Contains(salary))
            {
                panel2.Controls.Add(salary);
                salary.Dock = DockStyle.Fill;
                salary.BringToFront();
            }
            else
            {
                salary.BringToFront();
            }
        }

        private void btnAddclass_Click(object sender, EventArgs e)
        {
            
            if (!panel2.Controls.Contains(Class))
            {
                panel2.Controls.Add(Class);
                Class.Dock = DockStyle.Fill;
                Class.BringToFront();
            }
            else
            {
                Class.BringToFront();
            }
        }
    }
}
