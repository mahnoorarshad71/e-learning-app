﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using E_learning.BL;
using E_learning.DL;
namespace E_Learning
{
    public partial class SignIn : Form
    {
        MUser admin;
        public SignIn()
        {
            InitializeComponent();
            admin = new MUser("mahnoor", "123", "Admin");
            MUserDL.addUserIntoList(admin);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MUser m = new MUser(nametxt.Text,passwordtxt.Text);
            m=MUserDL.validUser(m);

            if (m != null)
            {
                nametxt.Clear();
                passwordtxt.Clear();
                if (MUserDL.isAdmin(m))
                {
                    MessageBox.Show("This is admin");
                  adminMenu a =  new adminMenu();
                    a.Show();
                }
                else if (MUserDL.isStudent(m))
                { 
                    MessageBox.Show("This is student");
                    Student n= StudentDL.validstudent(m);
                    if (m!=null)
                    {
                        studentMenu a = new studentMenu(n);
                        a.Show();
                    }
                }
                else if (MUserDL.isTeacher(m))
                {
                    Teacher t = TeacherDL.validteacher(m);
                    MessageBox.Show("This is teacher");
                    teacherMenu a = new teacherMenu(t);
                    a.Show();
                }
            }
            else
            {
                MessageBox.Show("Invalid username or password");
            }

        }

        private void SignUplbl_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            SignUp signUp = new SignUp();
            signUp.Show();
        }
    }
}
