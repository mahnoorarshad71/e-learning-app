﻿namespace E_Learning
{
    partial class addClass
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtdate = new System.Windows.Forms.TextBox();
            this.txtcode = new System.Windows.Forms.TextBox();
            this.txttime = new System.Windows.Forms.TextBox();
            this.code = new System.Windows.Forms.Label();
            this.lbltime = new System.Windows.Forms.Label();
            this.lbldate = new System.Windows.Forms.Label();
            this.btnaddCourse = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtdate
            // 
            this.txtdate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdate.Location = new System.Drawing.Point(70, 81);
            this.txtdate.Name = "txtdate";
            this.txtdate.Size = new System.Drawing.Size(248, 30);
            this.txtdate.TabIndex = 50;
            this.txtdate.TextChanged += new System.EventHandler(this.txtname_TextChanged);
            // 
            // txtcode
            // 
            this.txtcode.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcode.Location = new System.Drawing.Point(70, 226);
            this.txtcode.Name = "txtcode";
            this.txtcode.Size = new System.Drawing.Size(248, 30);
            this.txtcode.TabIndex = 49;
            this.txtcode.TextChanged += new System.EventHandler(this.txtID_TextChanged);
            // 
            // txttime
            // 
            this.txttime.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttime.Location = new System.Drawing.Point(70, 153);
            this.txttime.Name = "txttime";
            this.txttime.Size = new System.Drawing.Size(248, 30);
            this.txttime.TabIndex = 47;
            this.txttime.TextChanged += new System.EventHandler(this.txtdescription_TextChanged);
            // 
            // code
            // 
            this.code.AutoSize = true;
            this.code.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.code.Location = new System.Drawing.Point(65, 198);
            this.code.Name = "code";
            this.code.Size = new System.Drawing.Size(111, 25);
            this.code.TabIndex = 44;
            this.code.Text = "Enter Code";
            this.code.Click += new System.EventHandler(this.label2_Click);
            // 
            // lbltime
            // 
            this.lbltime.AutoSize = true;
            this.lbltime.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltime.Location = new System.Drawing.Point(65, 125);
            this.lbltime.Name = "lbltime";
            this.lbltime.Size = new System.Drawing.Size(107, 25);
            this.lbltime.TabIndex = 43;
            this.lbltime.Text = "Enter Time";
            this.lbltime.Click += new System.EventHandler(this.lblDescription_Click);
            // 
            // lbldate
            // 
            this.lbldate.AutoSize = true;
            this.lbldate.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldate.Location = new System.Drawing.Point(65, 53);
            this.lbldate.Name = "lbldate";
            this.lbldate.Size = new System.Drawing.Size(104, 25);
            this.lbldate.TabIndex = 42;
            this.lbldate.Text = "Enter Date";
            this.lbldate.Click += new System.EventHandler(this.lblname_Click);
            // 
            // btnaddCourse
            // 
            this.btnaddCourse.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddCourse.Location = new System.Drawing.Point(466, 412);
            this.btnaddCourse.Name = "btnaddCourse";
            this.btnaddCourse.Size = new System.Drawing.Size(118, 37);
            this.btnaddCourse.TabIndex = 51;
            this.btnaddCourse.Text = "Add";
            this.btnaddCourse.UseVisualStyleBackColor = true;
            this.btnaddCourse.Click += new System.EventHandler(this.btnaddCourse_Click);
            // 
            // addClass
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.Controls.Add(this.btnaddCourse);
            this.Controls.Add(this.txtdate);
            this.Controls.Add(this.txtcode);
            this.Controls.Add(this.txttime);
            this.Controls.Add(this.code);
            this.Controls.Add(this.lbltime);
            this.Controls.Add(this.lbldate);
            this.Name = "addClass";
            this.Size = new System.Drawing.Size(629, 479);
            this.Load += new System.EventHandler(this.addClass_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtdate;
        private System.Windows.Forms.TextBox txtcode;
        private System.Windows.Forms.TextBox txttime;
        private System.Windows.Forms.Label code;
        private System.Windows.Forms.Label lbltime;
        private System.Windows.Forms.Label lbldate;
        private System.Windows.Forms.Button btnaddCourse;
    }
}
