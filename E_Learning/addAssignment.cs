﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using E_learning.BL;
using E_learning.DL;
namespace E_Learning
{
    public partial class addAssignment : UserControl
    {
        Teacher t;
        public addAssignment(Teacher t)
        {
            this.t= t;
            InitializeComponent();
        }

        private void btnaddCourse_Click(object sender, EventArgs e)
        {
            try
            {
                Assignment a = new Assignment(txtname.Text, txtdescription.Text, txtduedate.Text, int.Parse(txtTMarks.Text));
                t.teachingCourse.assignments.Add(a);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
