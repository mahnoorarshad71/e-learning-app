﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using E_learning.BL;
using E_learning.DL;

namespace E_Learning
{
    public partial class SignUp : Form
    {
        public SignUp()
        {
            InitializeComponent();
        }

      

        private void btn1_Click(object sender, EventArgs e)
        {
            string role = null; ;
            if(chkstudent.Checked)
            {
                role =chkstudent.Text;
            }
            else if(chkteacher.Checked)
            {
                role=chkteacher.Text;
            }
            if (MUserDL.UserExists(nametxt.Text)==false)
            {
                if (role != null)
                {
                    MUser m = new MUser(nametxt.Text, passwordtxt.Text, role);
                    MUserDL.storeUserIntoFile(m);
                    MessageBox.Show("Registered");
                    if (MUserDL.isStudent(m))
                    {
                        Student s = new Student(m.name, m.password);
                        StudentDL.storeStudentintoFile(s);
                        StudentDL.addStudentIntoList(s);
                    }
                    else if (MUserDL.isTeacher(m))
                    {
                        Teacher t = new Teacher(m.name, m.password);
                        TeacherDL.addTeacherIntoList(t);
                    }
                }
            }
            else
            {
                MessageBox.Show("User already exists");
            }
           
        }

        private void chkteacher_CheckedChanged(object sender, EventArgs e)
        {
            if(chkstudent.Checked)
            {
                chkstudent.Checked = false;
            }
        }

        private void chkstudent_CheckedChanged(object sender, EventArgs e)
        {
            if (chkteacher.Checked)
            {
                chkteacher.Checked = false;
            }
        }
    }
}
