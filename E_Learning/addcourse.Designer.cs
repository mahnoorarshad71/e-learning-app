﻿namespace E_Learning
{
    partial class addcourse
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblname = new System.Windows.Forms.Label();
            this.lblDescription = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblprice = new System.Windows.Forms.Label();
            this.lblseats = new System.Windows.Forms.Label();
            this.txtdescription = new System.Windows.Forms.TextBox();
            this.txtprice = new System.Windows.Forms.TextBox();
            this.txtseats = new System.Windows.Forms.TextBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.txtname = new System.Windows.Forms.TextBox();
            this.btnaddCourse = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblname
            // 
            this.lblname.AutoSize = true;
            this.lblname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblname.Location = new System.Drawing.Point(58, 24);
            this.lblname.Name = "lblname";
            this.lblname.Size = new System.Drawing.Size(115, 25);
            this.lblname.TabIndex = 32;
            this.lblname.Text = "Enter Name";
            // 
            // lblDescription
            // 
            this.lblDescription.AutoSize = true;
            this.lblDescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDescription.Location = new System.Drawing.Point(58, 96);
            this.lblDescription.Name = "lblDescription";
            this.lblDescription.Size = new System.Drawing.Size(160, 25);
            this.lblDescription.TabIndex = 33;
            this.lblDescription.Text = "Enter Description";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(58, 169);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 25);
            this.label2.TabIndex = 34;
            this.label2.Text = "Enter ID";
            // 
            // lblprice
            // 
            this.lblprice.AutoSize = true;
            this.lblprice.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblprice.Location = new System.Drawing.Point(58, 314);
            this.lblprice.Name = "lblprice";
            this.lblprice.Size = new System.Drawing.Size(107, 25);
            this.lblprice.TabIndex = 35;
            this.lblprice.Text = "Enter Price";
            // 
            // lblseats
            // 
            this.lblseats.AutoSize = true;
            this.lblseats.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblseats.Location = new System.Drawing.Point(58, 242);
            this.lblseats.Name = "lblseats";
            this.lblseats.Size = new System.Drawing.Size(119, 25);
            this.lblseats.TabIndex = 36;
            this.lblseats.Text = "Enter Seats ";
            // 
            // txtdescription
            // 
            this.txtdescription.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtdescription.Location = new System.Drawing.Point(63, 124);
            this.txtdescription.Name = "txtdescription";
            this.txtdescription.Size = new System.Drawing.Size(248, 30);
            this.txtdescription.TabIndex = 37;
            // 
            // txtprice
            // 
            this.txtprice.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtprice.Location = new System.Drawing.Point(63, 342);
            this.txtprice.Name = "txtprice";
            this.txtprice.Size = new System.Drawing.Size(248, 30);
            this.txtprice.TabIndex = 38;
            // 
            // txtseats
            // 
            this.txtseats.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtseats.Location = new System.Drawing.Point(63, 270);
            this.txtseats.Name = "txtseats";
            this.txtseats.Size = new System.Drawing.Size(248, 30);
            this.txtseats.TabIndex = 39;
            // 
            // txtID
            // 
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.Location = new System.Drawing.Point(63, 197);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(248, 30);
            this.txtID.TabIndex = 40;
            // 
            // txtname
            // 
            this.txtname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.Location = new System.Drawing.Point(63, 52);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(248, 30);
            this.txtname.TabIndex = 41;
            // 
            // btnaddCourse
            // 
            this.btnaddCourse.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddCourse.Location = new System.Drawing.Point(474, 424);
            this.btnaddCourse.Name = "btnaddCourse";
            this.btnaddCourse.Size = new System.Drawing.Size(118, 37);
            this.btnaddCourse.TabIndex = 42;
            this.btnaddCourse.Text = "Add";
            this.btnaddCourse.UseVisualStyleBackColor = true;
            this.btnaddCourse.Click += new System.EventHandler(this.btnregister_Click);
            // 
            // addcourse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnaddCourse);
            this.Controls.Add(this.txtname);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.txtseats);
            this.Controls.Add(this.txtprice);
            this.Controls.Add(this.txtdescription);
            this.Controls.Add(this.lblseats);
            this.Controls.Add(this.lblprice);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblDescription);
            this.Controls.Add(this.lblname);
            this.Name = "addcourse";
            this.Size = new System.Drawing.Size(629, 479);
            this.Load += new System.EventHandler(this.addcourse_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblname;
        private System.Windows.Forms.Label lblDescription;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblprice;
        private System.Windows.Forms.Label lblseats;
        private System.Windows.Forms.TextBox txtdescription;
        private System.Windows.Forms.TextBox txtprice;
        private System.Windows.Forms.TextBox txtseats;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.Button btnaddCourse;
    }
}
