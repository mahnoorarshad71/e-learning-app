﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using E_learning.BL;
using E_learning.DL;

namespace E_Learning
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            CourseDL.loadcourse();
            StudentDL.loadStudent();
           TeacherDL.loadTeacher();


            MUserDL.loadUsers();
           
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new SignIn());
        }
    }
}
