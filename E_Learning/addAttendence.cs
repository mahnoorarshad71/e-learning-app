﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using E_learning.BL;
using E_learning.DL;
namespace E_Learning
{
    public partial class addAttendence : UserControl
    {
        Teacher t;
       
        public addAttendence(Teacher t)
        {
            this.t = t;
            InitializeComponent();
        }
        
        private void studentsattendenceGV_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            Student student = (Student)studentsattendenceGV.CurrentRow.DataBoundItem;
            if (studentsattendenceGV.Columns["Attendence"].Index == e.ColumnIndex)
            {
                addstudentAttendence myform = new addstudentAttendence(student);
                myform.ShowDialog();
               StudentDL.storeStudentintoFile(student);
               
            }
          
        }
    }
}
