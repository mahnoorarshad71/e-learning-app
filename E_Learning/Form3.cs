﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using E_learning.BL;
using E_learning.DL;
namespace E_Learning
{
    public partial class adminMenu : Form
    {
        addcourse userControl;
        viewCourse course;
        viewTeacher teachers;
        home homeui;
        viewStudent student;

        public adminMenu()
        {
            InitializeComponent();
            userControl = new addcourse();
            course = new viewCourse();
            teachers = new viewTeacher();
            student = new viewStudent();


        }
        private void Form3_Load(object sender, EventArgs e)
        {
            course.coursesGV.DataSource = CourseDL.course;
            teachers.teachersGV.DataSource = TeacherDL.teacher;
            student.studentsGV.DataSource = StudentDL.students;
           
            
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {

        }
       
        private void button1_Click(object sender, EventArgs e)
        {
            if (!panel2.Controls.Contains(homeui))
            {
                panel2.Controls.Add(homeui);
                homeui.BringToFront();
            }
            else
            {
                homeui.BringToFront();
            }


        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (!panel2.Controls.Contains(userControl))
            {
                panel2.Controls.Add(userControl);
                userControl.Dock = DockStyle.Fill;
                userControl.BringToFront();
            }
            else
            {
                userControl.BringToFront();
            }
        }
        

        private void button7_Click(object sender, EventArgs e)
        {
            if (!panel2.Controls.Contains(course))
            {
                panel2.Controls.Add(course);
                course.Dock = DockStyle.Fill;
                course.BringToFront();
            }
            else
            {
                course.BringToFront();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button9_Click(object sender, EventArgs e)
        {
            CourseDL.storeCourse();
            this.Close();
        }

        private void button8_Click(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {

        }
       
        private void button4_Click(object sender, EventArgs e)
        {
            if (!panel2.Controls.Contains(student))
            {
                panel2.Controls.Add(student);
                student.Dock = DockStyle.Fill;
                student.BringToFront();
            }
            else
            {
                student.BringToFront();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (!panel2.Controls.Contains(teachers))
            {
                panel2.Controls.Add(teachers);
                teachers.Dock = DockStyle.Fill;
                teachers.BringToFront();
            }
            else
            {
                teachers.BringToFront();
            }
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
