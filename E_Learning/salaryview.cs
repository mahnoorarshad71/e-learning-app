﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using E_learning.BL;
using E_learning.DL;
namespace E_Learning
{
    public partial class salaryview : UserControl
    {
        Teacher t;
        public salaryview(Teacher t)
        {
            this.t = t;
            InitializeComponent();
        }

       

        private void salaryview_Load(object sender, EventArgs e)
        {
            lblsalary.Text=t.Salary.ToString();
        }
    }
}
