﻿namespace E_Learning
{
    partial class addinfo
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnaddteacher = new System.Windows.Forms.Button();
            this.txtname = new System.Windows.Forms.TextBox();
            this.txtphone = new System.Windows.Forms.TextBox();
            this.txtID = new System.Windows.Forms.TextBox();
            this.txtemail = new System.Windows.Forms.TextBox();
            this.txtqualification = new System.Windows.Forms.TextBox();
            this.lblID = new System.Windows.Forms.Label();
            this.lblemail = new System.Windows.Forms.Label();
            this.lblphone = new System.Windows.Forms.Label();
            this.lblQualification = new System.Windows.Forms.Label();
            this.lblname = new System.Windows.Forms.Label();
            this.txtpassword = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnaddteacher
            // 
            this.btnaddteacher.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddteacher.Location = new System.Drawing.Point(463, 421);
            this.btnaddteacher.Name = "btnaddteacher";
            this.btnaddteacher.Size = new System.Drawing.Size(125, 37);
            this.btnaddteacher.TabIndex = 53;
            this.btnaddteacher.Text = "Add/Update";
            this.btnaddteacher.UseVisualStyleBackColor = true;
            this.btnaddteacher.Click += new System.EventHandler(this.btnaddCourse_Click);
            // 
            // txtname
            // 
            this.txtname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.Location = new System.Drawing.Point(52, 49);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(248, 30);
            this.txtname.TabIndex = 52;
            // 
            // txtphone
            // 
            this.txtphone.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtphone.Location = new System.Drawing.Point(52, 194);
            this.txtphone.Name = "txtphone";
            this.txtphone.Size = new System.Drawing.Size(248, 30);
            this.txtphone.TabIndex = 51;
            // 
            // txtID
            // 
            this.txtID.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtID.Location = new System.Drawing.Point(52, 267);
            this.txtID.Name = "txtID";
            this.txtID.Size = new System.Drawing.Size(248, 30);
            this.txtID.TabIndex = 50;
            // 
            // txtemail
            // 
            this.txtemail.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtemail.Location = new System.Drawing.Point(52, 339);
            this.txtemail.Name = "txtemail";
            this.txtemail.Size = new System.Drawing.Size(248, 30);
            this.txtemail.TabIndex = 49;
            // 
            // txtqualification
            // 
            this.txtqualification.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtqualification.Location = new System.Drawing.Point(52, 121);
            this.txtqualification.Name = "txtqualification";
            this.txtqualification.Size = new System.Drawing.Size(248, 30);
            this.txtqualification.TabIndex = 48;
            // 
            // lblID
            // 
            this.lblID.AutoSize = true;
            this.lblID.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblID.Location = new System.Drawing.Point(47, 239);
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(87, 25);
            this.lblID.TabIndex = 47;
            this.lblID.Text = "Enter ID ";
            // 
            // lblemail
            // 
            this.lblemail.AutoSize = true;
            this.lblemail.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblemail.Location = new System.Drawing.Point(47, 311);
            this.lblemail.Name = "lblemail";
            this.lblemail.Size = new System.Drawing.Size(111, 25);
            this.lblemail.TabIndex = 46;
            this.lblemail.Text = "Enter Email";
            // 
            // lblphone
            // 
            this.lblphone.AutoSize = true;
            this.lblphone.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblphone.Location = new System.Drawing.Point(47, 166);
            this.lblphone.Name = "lblphone";
            this.lblphone.Size = new System.Drawing.Size(120, 25);
            this.lblphone.TabIndex = 45;
            this.lblphone.Text = "Enter Phone";
            // 
            // lblQualification
            // 
            this.lblQualification.AutoSize = true;
            this.lblQualification.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblQualification.Location = new System.Drawing.Point(47, 93);
            this.lblQualification.Name = "lblQualification";
            this.lblQualification.Size = new System.Drawing.Size(170, 25);
            this.lblQualification.TabIndex = 44;
            this.lblQualification.Text = "Enter Qualification";
            // 
            // lblname
            // 
            this.lblname.AutoSize = true;
            this.lblname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblname.Location = new System.Drawing.Point(47, 21);
            this.lblname.Name = "lblname";
            this.lblname.Size = new System.Drawing.Size(115, 25);
            this.lblname.TabIndex = 43;
            this.lblname.Text = "Enter Name";
            // 
            // txtpassword
            // 
            this.txtpassword.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtpassword.Location = new System.Drawing.Point(52, 421);
            this.txtpassword.Name = "txtpassword";
            this.txtpassword.Size = new System.Drawing.Size(248, 30);
            this.txtpassword.TabIndex = 55;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(47, 384);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 25);
            this.label1.TabIndex = 54;
            this.label1.Text = "Enter Password";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // addinfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtpassword);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnaddteacher);
            this.Controls.Add(this.txtname);
            this.Controls.Add(this.txtphone);
            this.Controls.Add(this.txtID);
            this.Controls.Add(this.txtemail);
            this.Controls.Add(this.txtqualification);
            this.Controls.Add(this.lblID);
            this.Controls.Add(this.lblemail);
            this.Controls.Add(this.lblphone);
            this.Controls.Add(this.lblQualification);
            this.Controls.Add(this.lblname);
            this.Name = "addinfo";
            this.Size = new System.Drawing.Size(629, 479);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnaddteacher;
        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.TextBox txtphone;
        private System.Windows.Forms.TextBox txtID;
        private System.Windows.Forms.TextBox txtemail;
        private System.Windows.Forms.TextBox txtqualification;
        private System.Windows.Forms.Label lblID;
        private System.Windows.Forms.Label lblemail;
        private System.Windows.Forms.Label lblphone;
        private System.Windows.Forms.Label lblQualification;
        private System.Windows.Forms.Label lblname;
        private System.Windows.Forms.TextBox txtpassword;
        private System.Windows.Forms.Label label1;
    }
}
