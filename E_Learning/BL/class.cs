﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_learning.BL
{
     public class @class
    {
       
        public @class(string date, string time,string code)
        {
            this.date = date;
            this.time = time;
            this.code = code;
        }
        public string date
        { get; set; }
        public string time
        { get; set; }   
        public string code
        { get; set; }
    }
}
