﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_learning.BL
{
    public class Teacher : MUser
    {
        public Teacher(string Name, string password) : base(Name, password)
        {
        }
        public string ID
        {get; set;}
        public string Email
        { get; set;}
        public string Qualification
        { get; set;}
        public List<Course> appliedcourses = new List<Course>();
        public Course teachingCourse;
        public float Salary;
        public string teacher;
        public bool jobStatus;
        public ulong Phone;
        
       

    }
}
