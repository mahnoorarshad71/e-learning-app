﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_learning.BL
{
    public class MUser
    {
        
        public MUser(string name,string password, string role)
        {
            this.name = name;
            this.password = password;
            this.role = role;
        }
        public MUser(string name, string password)
        {
            this.name = name;
            this.password = password;
        }


        public string name
        { get; set; }
        public string password
        { get; set; }
        public string role
        { get; set; }
    }
}
