﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using E_learning.BL;
using E_learning.DL;
namespace E_Learning
{
    public partial class studentMenu : Form
    {
        Student student;
        viewCourse course;
        home homeui;
        public studentMenu(Student student)
        {
            this.student = student;
            InitializeComponent();
            homeui = new home();
            course = new viewCourse();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!panel2.Controls.Contains(homeui))
            {
                panel2.Controls.Add(homeui);
                homeui.Dock = DockStyle.Fill;
                homeui.BringToFront();
            }
            else
            {
                homeui.BringToFront();
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            StudentDL.storeStudentintoFile(student);
            this.Close();
        }
    }
}
