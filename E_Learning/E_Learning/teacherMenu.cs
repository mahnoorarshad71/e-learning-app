﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace E_Learning
{
    public partial class teacherMenu : Form
    {
        home homeui;
        public teacherMenu()
        {
            InitializeComponent();
            homeui = new home();
        }

        private void teacherMenu_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!panel2.Controls.Contains(homeui))
            {
                panel2.Controls.Add(homeui);
                homeui.Dock = DockStyle.Fill;
                homeui.BringToFront();
            }
            else
            {
                homeui.BringToFront();
            }

        }
    }
}
