﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using E_learning.BL;
using System.IO;
namespace E_learning.DL
{
    internal class MUserDL
    {
        public static List<MUser> users = new List<MUser>();
        public static string path = "C:\\Users\\M\\source\\repos\\E_learning\\Users.txt";
        public static  bool isAdmin(MUser user)
        {
            if (user.role == "Admin")
            { return true; }
            return false;
        }
        public static bool isStudent(MUser user)
        {
            if (user.role == "Student")
            { return true; }
            return false;
        }
        public static bool isTeacher(MUser user)
        {
            if (user.role == "Teacher")
            { return true; }
            return false;
        }

        public static void addUserIntoList(MUser user)
        {
            users.Add(user);
        }
        public static void storeUserIntoFile(MUser user)
        {
            StreamWriter Users = new StreamWriter(path, true);
            Users.WriteLine(user.name + "," + user.password + "," + user.role);
            Users.Flush();
            Users.Close();
        }
        public static void loadUsers()
        {
            StreamReader Users = new StreamReader(path);
            string line;
            
            string[] parts = new string[3];
            string name, password, role;
            while ((line = Users.ReadLine()) != null) 
            {
                 parts= line.Split(',');
                name = parts[0];
               password = parts[1];
                role = parts[2];
                MUser user = new MUser(name,password,role);
                users.Add(user);
              
            }
            
            Users.Close();
        }
        public static bool UserExists(string name)
        {
            foreach (MUser m in users)
            {
                if (m.name == name)
                { return true; }
            }
            return false;
        }
        public static MUser validUser(MUser n)
        {
            foreach (MUser m in users)
            {
                if (m.name == n.name &&m.password==n.password)
                { return m; }
            }
            return null;
        }
    }
}
