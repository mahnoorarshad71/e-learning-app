﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using E_learning.BL;
namespace E_learning.DL
{
    internal class StudentDL
    {
        public static List<Student> students = new List<Student>();
        public static string path = "C:\\Users\\M\\source\\repos\\E_learning\\students.txt";
        public static void storeStudentintoFile( Student s)
        {
            StreamWriter f = new StreamWriter(path, true);
            string regiseredcourses = "";
            try
            {

                for (int x = 0; x < s.registeredCourses.Count - 1; x++)
                {
                    regiseredcourses = regiseredcourses + s.registeredCourses[x].ID + ";";
                }
                regiseredcourses = regiseredcourses + s.registeredCourses[s.registeredCourses.Count - 1].ID;
            }
            catch (Exception ex)
            { Console.WriteLine(ex.ToString()); }
            f.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7}", s.name, s.password, s.Level, s.Payment, s.Phone,s.attendence, s.Grade,regiseredcourses);
            f.Flush();
            f.Close();
        }
        static void storeStudentListInFile()
        {
            StreamWriter student = new StreamWriter(path, true);
            foreach (Student j in students)
            {
                storeStudentintoFile(j);
            }
            student.Flush();
            student.Close();
        }
        public static void addStudentIntoList(Student s)
        {
            students.Add(s);
        }

        public static bool loadStudent()
        {
            StreamReader f = new StreamReader(path);
            string record;
            if (File.Exists(path))
            {
                while ((record = f.ReadLine()) != null)
                {
                    string[] splittedRecord = record.Split(',');
                    string name = splittedRecord[0];
                    string password = splittedRecord[1];
                    string level = splittedRecord[2];
                    float payment = float.Parse(splittedRecord[3]);
                    ulong phone = ulong.Parse(splittedRecord[4]);
                    float atttendence = float.Parse(splittedRecord[5]);
                    string grade = splittedRecord[6];
                    string[] splittedRecordForCourses = splittedRecord[7].Split(';');
                    List<Course> courses = new List<Course>();
                    for (int x = 0; x < splittedRecordForCourses.Length; x++)
                    {
                        Course d = CourseDL.getCourse(splittedRecordForCourses[x]);
                        if (d != null)
                        {
                            if (!(courses.Contains(d)))
                                courses.Add(d);
                        }
                    }
                    Student s = new Student(name, password);
                    s.attendence = atttendence;
                    s.Payment= payment;
                    s.registeredCourses = courses;
                    s.Phone= phone;
                    s.Level= level;
                    s.Grade= grade;
                }
                f.Close();
                return true;
            }
            else
                return false;
        }
     
        public static Student validstudent(MUser m)
        {

            foreach(var student in students)
            {
                if(m.name==student.name&&m.password==student.password)
                {
                    return student;
                }
            }
            return null;
        }
    }
}
