﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace elearning.BL
{
    internal class Question
    {
        public Question(string statement,List<string> Options)
        {
            this.statement = statement;

            options = new List<string>();
            options = Options;
        }
        public string statement
            { get; set; }
        public List<string> options;
        public string answer
        { get; set; }
        
    }

}
