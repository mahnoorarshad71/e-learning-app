﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_learning.BL
{
   public class Assignment
    {
        public Assignment(string name, string description,string Duedate,int Tmarks )
        {
            this.name = name;
            this.description = description;
            this.Duedate = Duedate; 
            this.TMarks = Tmarks;
        }
        public string name
        { get;set; }
        public string description
        { get; set; }
        public string Duedate
        { get; set; }
        public int TMarks
        { get; set; }
        public int Omarks
        { get; set; }
    }
}
