﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E_learning.BL
{
    public class Course
    {
        public string Name
        { get; set; }
        public string ID;
        public string Description
        { get; set; }
        public float price;
        public Teacher teacher;
        public int T_status
        { get; set; }
        public int sLimit
        { get; set; }
        // number of student seats
        public int Snum
        { get; set; }//number of students registered in course
        
        public List<@class> classes;
        public List<Assignment> assignments;
        public float totalattendence = 0;
        public Course(string Name,string ID,string Description,float price,int sLimit)
            {
            this.Name = Name;
            this.ID = ID;
            this.Description = Description;
            this.price = price;
            this.sLimit = sLimit;
            classes = new List<@class>();
            assignments = new List<Assignment>();
            }
       public void addAssignment(Assignment a)
        {
            assignments.Add(a);
        }



    }
}
