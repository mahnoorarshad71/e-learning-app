﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using E_learning.BL;
using E_learning.DL;

namespace E_Learning
{
    static class Program
    {
        [STAThread]
        static void Main()
        {
            CourseDL.loadcourse();
            TeacherDL.loadTeacher();
            MUserDL.loadUsers();
            StudentDL.loadStudent();
            Course c = new Course("Web development", "123", "2 months", 1200, 10);
            CourseDL.course.Add(c);
            Course n = new Course("Digital Marketing", "1234", "2 months", 1200, 10);
            CourseDL.course.Add(n);
            Student s = new Student("mehwish", "123");
            s.registeredCourses.Add(c);
            s.registeredCourses.Add(n);
            s.Payment = 1200;
            s.Phone = 92114412825;
            s.attendence = 10;
            s.Grade = "A";
            s.Level = "inter";
            StudentDL.addStudentIntoList(s);
            //udentDL.storeStudentintoFile(s);
            c.totalattendence = 10;
            Teacher t = new Teacher("hafsa", "123");
            t.teachingCourse = c;
            t.jobStatus = true;
            t.Email = "mahnoorarshad71@gmail.com";
            t.Salary = 12000;
            t.Qualification = "BSCS";
            t.ID = "3410144309876";
            TeacherDL.teacher.Add(t);
           //eacherDL.storeteacher();
            //urseDL.storeCourse();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new SignIn());
        }
    }
}
