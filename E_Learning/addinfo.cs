﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using E_learning.BL;
using E_learning.DL;
namespace E_Learning
{
    public partial class addinfo : UserControl
    {
        Teacher t;
        public addinfo(Teacher t)
        {
            InitializeComponent();
            this.t = t;
        }

        private void btnaddCourse_Click(object sender, EventArgs e)
        {
            try
            {
                t.Email = txtemail.Text;
                t.password = txtpassword.Text;
                t.name= txtname.Text;
                t.ID= txtID.Text;
                t.Phone = ulong.Parse(txtphone.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
