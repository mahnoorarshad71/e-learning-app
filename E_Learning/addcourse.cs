﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using E_learning.BL;
using E_learning.DL;

namespace E_Learning
{
    public partial class addcourse : UserControl
    {
        public addcourse()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
           this.BackColor = Color.Gray;
        }

        private void btnregister_Click(object sender, EventArgs e)
        {
            try
            {
                Course c = new Course(txtname.Text, txtID.Text, txtdescription.Text, float.Parse(txtprice.Text), int.Parse(txtseats.Text));
                CourseDL.course.Add(c);
                MessageBox.Show("added");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void addcourse_Load(object sender, EventArgs e)
        {

        }
    }
}
