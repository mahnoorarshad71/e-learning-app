﻿namespace E_Learning
{
    partial class addstudentAttendence
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtname = new System.Windows.Forms.TextBox();
            this.lblname = new System.Windows.Forms.Label();
            this.btnaddCourse = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtname
            // 
            this.txtname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtname.Location = new System.Drawing.Point(67, 95);
            this.txtname.Name = "txtname";
            this.txtname.Size = new System.Drawing.Size(248, 30);
            this.txtname.TabIndex = 43;
            // 
            // lblname
            // 
            this.lblname.AutoSize = true;
            this.lblname.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblname.Location = new System.Drawing.Point(62, 67);
            this.lblname.Name = "lblname";
            this.lblname.Size = new System.Drawing.Size(163, 25);
            this.lblname.TabIndex = 42;
            this.lblname.Text = "Enter Attendence";
            // 
            // btnaddCourse
            // 
            this.btnaddCourse.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnaddCourse.Location = new System.Drawing.Point(267, 180);
            this.btnaddCourse.Name = "btnaddCourse";
            this.btnaddCourse.Size = new System.Drawing.Size(118, 37);
            this.btnaddCourse.TabIndex = 44;
            this.btnaddCourse.Text = "Add";
            this.btnaddCourse.UseVisualStyleBackColor = true;
            this.btnaddCourse.Click += new System.EventHandler(this.btnaddCourse_Click);
            // 
            // addstudentAttendence
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(397, 229);
            this.Controls.Add(this.btnaddCourse);
            this.Controls.Add(this.txtname);
            this.Controls.Add(this.lblname);
            this.Name = "addstudentAttendence";
            this.Text = "Form5";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtname;
        private System.Windows.Forms.Label lblname;
        private System.Windows.Forms.Button btnaddCourse;
    }
}