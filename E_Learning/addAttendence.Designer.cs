﻿namespace E_Learning
{
    partial class addAttendence
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.studentsattendenceGV = new System.Windows.Forms.DataGridView();
            this.Attendence = new System.Windows.Forms.DataGridViewButtonColumn();
            ((System.ComponentModel.ISupportInitialize)(this.studentsattendenceGV)).BeginInit();
            this.SuspendLayout();
            // 
            // studentsattendenceGV
            // 
            this.studentsattendenceGV.AllowUserToAddRows = false;
            this.studentsattendenceGV.AllowUserToResizeColumns = false;
            this.studentsattendenceGV.AllowUserToResizeRows = false;
            this.studentsattendenceGV.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.studentsattendenceGV.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.studentsattendenceGV.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.studentsattendenceGV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.studentsattendenceGV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Attendence});
            this.studentsattendenceGV.Dock = System.Windows.Forms.DockStyle.Right;
            this.studentsattendenceGV.Location = new System.Drawing.Point(0, 0);
            this.studentsattendenceGV.Margin = new System.Windows.Forms.Padding(2);
            this.studentsattendenceGV.Name = "studentsattendenceGV";
            this.studentsattendenceGV.RowHeadersWidth = 62;
            this.studentsattendenceGV.RowTemplate.DefaultCellStyle.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.studentsattendenceGV.RowTemplate.Height = 28;
            this.studentsattendenceGV.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.studentsattendenceGV.ShowEditingIcon = false;
            this.studentsattendenceGV.Size = new System.Drawing.Size(629, 479);
            this.studentsattendenceGV.TabIndex = 4;
            this.studentsattendenceGV.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.studentsattendenceGV_CellContentClick);
            // 
            // Attendence
            // 
            this.Attendence.HeaderText = "Add Attendence";
            this.Attendence.Name = "Attendence";
            this.Attendence.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Attendence.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // addAttendence
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.studentsattendenceGV);
            this.Name = "addAttendence";
            this.Size = new System.Drawing.Size(629, 479);
            ((System.ComponentModel.ISupportInitialize)(this.studentsattendenceGV)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.DataGridView studentsattendenceGV;
        private System.Windows.Forms.DataGridViewButtonColumn Attendence;
    }
}
